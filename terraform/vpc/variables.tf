# AWS
variable "aws_region" {
  type = string
}

# Project
variable "project" {
  type = string
}

variable "company" {
  type = string
}

variable "environment" {
  type = string
}

# Networking
variable "vpc_cidr_block" {
  type = string
}

variable "subnet_public_1" {
  type = string
}

variable "subnet_private_1" {
  type = string
}

variable "subnet_public_2" {
  type = string
}

variable "subnet_private_2" {
  type = string
}
