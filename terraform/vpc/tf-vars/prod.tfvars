# AWS
aws_region = "us-east-1"

# Project
project     = "terraform-learning"
company     = "ceneval"
environment = "prod"

# Networking
vpc_cidr_block = "10.10.0.0/16" # 65534 hosts
subnet_public_1  = "10.10.1.0/24" # 254 hosts
subnet_public_2 = "10.10.2.0/24" # 254 hosts
subnet_private_1 = "10.10.3.0/24" # 254 hosts
subnet_private_2 = "10.10.4.0/24" # 254 hosts