# AWS
aws_region = "us-east-1"

# Project
project     = "terraform-learning"
company     = "ceneval"
environment = "training"

# Networking
vpc_cidr_block = "172.16.0.0/16" # 65534 hosts
subnet_public  = "172.16.1.0/24" # 254 hosts
subnet_private = "172.16.2.0/24" # 254 hosts