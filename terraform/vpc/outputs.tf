
output "vpc_cidr" {
  value = aws_vpc.main.cidr_block
}

output "public_cidr_1" {
  value = aws_subnet.public_1.cidr_block
}

output "public_cidr_2" {
  value = aws_subnet.public_2.cidr_block
}

output "private_cidr_1" {
  value = aws_subnet.private_1.cidr_block
}

output "private_cidr_2" {
  value = aws_subnet.private_2.cidr_block
}
