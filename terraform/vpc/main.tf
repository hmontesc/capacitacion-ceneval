# VPC & Subnets
# --------------------------------------

resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name = "${var.environment}-vpc"
  }
}

# Availability Zone 1

resource "aws_subnet" "public_1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet_public_1
  availability_zone       = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.environment}-public_1"
  }
}

resource "aws_subnet" "private_1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_private_1
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "${var.environment}-private_1"
  }
}

# Availability Zone 2

resource "aws_subnet" "public_2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet_public_2
  availability_zone       = data.aws_availability_zones.available.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.environment}-public_2"
  }
}

resource "aws_subnet" "private_2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_private_2
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "${var.environment}-private_2"
  }
}

# Internet Gateway
# --------------------------------------

resource "aws_internet_gateway" "terraform_internet_gateway" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.environment}-igw"
  }
}

# Public Route Table
# --------------------------------------

resource "aws_route_table" "terraform_public_subnet_routetable" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terraform_internet_gateway.id
  }

  tags = {
    Name = "${var.environment}-public_rt"
  }
}

resource "aws_route_table_association" "terraform_public_subnet_routetable_assoc_1" {
  subnet_id      = aws_subnet.public_1.id
  route_table_id = aws_route_table.terraform_public_subnet_routetable.id
}

resource "aws_route_table_association" "terraform_public_subnet_routetable_assoc_2" {
  subnet_id      = aws_subnet.public_2.id
  route_table_id = aws_route_table.terraform_public_subnet_routetable.id
}


# Private Route Table
# --------------------------------------

resource "aws_default_route_table" "terraform_private_subnet_routetable" {
  default_route_table_id = aws_vpc.main.default_route_table_id

  tags = {
    Name = "${var.environment}-private_rt"
  }
}

resource "aws_route_table_association" "terraform_private_subnet_routetable_assoc_1" {
  subnet_id      = aws_subnet.private_1.id
  route_table_id = aws_default_route_table.terraform_private_subnet_routetable.id
}

resource "aws_route_table_association" "terraform_private_subnet_routetable_assoc_2" {
  subnet_id      = aws_subnet.private_2.id
  route_table_id = aws_default_route_table.terraform_private_subnet_routetable.id
}
