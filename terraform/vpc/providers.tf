provider "aws" {
  region = var.aws_region
  default_tags {
    tags = {
      Project     = var.project
      Environment = var.environment
      Company     = var.company
      Createdwith = "Terraform"
    }
  }
}