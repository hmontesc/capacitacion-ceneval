# AWS
aws_region = "us-east-1"

# Project
project     = "terraform-learning"
company     = "ceneval"
environment = "training"

# Bucket
bucket_name = "learningbucket"