provider "aws" {
  region = var.aws_region
  default_tags {
    tags = {
      environment = var.environment
      project     = var.project
      company     = var.company
      createdwith = "Terraform"
    }
  }
}