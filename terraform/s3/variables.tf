# AWS
variable "aws_region" {
  type = string
}

# Project
variable "project" {
  type = string
}

variable "company" {
  type = string
}

variable "environment" {
  type = string
}

# Bucket
variable "bucket_name" {
  type = string
}
