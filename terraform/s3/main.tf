resource "random_id" "name" {
  byte_length = 8
}

resource "aws_s3_bucket" "bucket" {
  bucket = "${random_id.name.hex}-${var.bucket_name}"
  acl    = "public-read"

  tags = {
    "Name" = "${var.environment}-${var.bucket_name}"
  }
  # website {
  #   index_document = "index.html"
  # }
}
