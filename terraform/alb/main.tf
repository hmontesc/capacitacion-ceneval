resource "aws_lb" "app" {
  name               = "${var.environment}-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = [data.aws_subnet.public.id, data.aws_subnet.public2.id]
  security_groups    = [data.aws_security_groups.vpc.ids[0]]

  tags = {
    "Name" = "${var.environment}-alb"
  }

}

resource "aws_lb_listener" "app" {
  depends_on        = [aws_alb_target_group.webserver]
  load_balancer_arn = aws_lb.app.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.webserver.arn
  }
}

resource "aws_alb_target_group" "webserver" {
  name     = "${var.environment}-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.selected.id

  health_check {
    port     = 80
    protocol = "HTTP"
    timeout  = 5
    interval = 10

    healthy_threshold   = 2
    unhealthy_threshold = 2
    path                = var.instance_server_path
  }

  tags = {
    Name = "${var.environment}-tg"
  }

}

resource "aws_launch_configuration" "lab_web_lc" {
  name_prefix                 = "web-"
  image_id                    = var.aws_ami
  instance_type               = var.aws_instance_type
  key_name                    = var.key_name
  security_groups             = [data.aws_security_groups.vpc.ids[0]]
  associate_public_ip_address = true

  user_data = <<-EOF
              #!/bin/bash
              yum update -y
              yum install -y httpd.x86_64
              systemctl start httpd.service
              systemctl enable httpd.service
              echo "<h1>This is $(hostname)</h1>" > /var/www/html/index.html
              EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "lab_web_asg" {
  name                 = "${var.environment}-asg"
  launch_configuration = aws_launch_configuration.lab_web_lc.id
  vpc_zone_identifier  = [data.aws_subnet.public.id, data.aws_subnet.public2.id]
  #   load_balancers    = [aws_lb.app.arn]
  target_group_arns = [aws_alb_target_group.webserver.id]

  min_size         = 2
  desired_capacity = 2
  max_size         = 3

  health_check_type = "ELB"

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  lifecycle {
    create_before_destroy = true
  }

  tags = [
    {
      key                 = "Name"
      value               = "${var.environment}-asg"
      propagate_at_launch = true
    }
  ]

}
