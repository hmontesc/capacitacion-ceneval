# AWS
aws_region = "us-east-1"

# Project
project     = "terraform-learning"
company     = "ceneval"
environment = "prod"

instance_server_path = "/index.html"

aws_instance_type = "t2.micro"
key_name          = "keyceneval"

aws_ami = "ami-0c2b8ca1dad447f8a"