# AWS
variable "aws_region" {
  type = string
}

# Project
variable "project" {
  type = string
}

variable "company" {
  type = string
}

variable "environment" {
  type = string
}


variable "instance_server_path" {
  type = string
}

variable "aws_ami" {
  type = string
}

variable "aws_instance_type" {
  type = string
}

variable "key_name" {
  type = string
}
