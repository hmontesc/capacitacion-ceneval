#  Output Load Balancer DNS
# --------------------------------------
output "alb_dns" {
  value = aws_lb.app.dns_name
}