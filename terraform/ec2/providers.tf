provider "aws" {
  #   alias  = "dns"
  region = var.aws_region
  default_tags {
    tags = {
      Environment = var.environment
      Project     = var.project
      Company     = var.company
      Createdwith = "Terraform"
    }
  }
}