# AWS
aws_region = "us-east-1"

# Project
project     = "udemy-learning"
company     = "ceneval"
environment = "prod"

# EC2 instance
aws_instance_type = "t2.micro"
key_name = "keyceneval"
