variable "aws_region" {
  type = string
}

variable "project" {
  type = string
}

variable "company" {
  type = string
}

variable "aws_instance_type" {
  type = string
}

variable "environment" {
  type = string
}

variable "key_name" {
  type = string
}

variable "aws_ami" {
  type = map(any)
  default = {
    "amazon" = "ami-0c2b8ca1dad447f8a"
    "ubuntu" = "ami-09e67e426f25ce0d7"
    "redhat" = "ami-0b0af3577fe5e3532"
  }
}
