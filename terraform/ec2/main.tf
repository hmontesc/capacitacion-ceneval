resource "aws_instance" "public" {
  ami                         = var.aws_ami.amazon
  associate_public_ip_address = true
  instance_type               = var.aws_instance_type
  key_name                    = var.key_name

  subnet_id              = data.aws_subnet.public.id
  vpc_security_group_ids = data.aws_security_groups.vpc.ids

  tags = {
    "Name" = "${var.environment}-public"
  }
}


resource "aws_instance" "private" {
  ami                         = var.aws_ami.amazon
  associate_public_ip_address = false
  instance_type               = var.aws_instance_type
  key_name                    = var.key_name

  subnet_id              = data.aws_subnet.private.id
  vpc_security_group_ids = data.aws_security_groups.vpc.ids

  tags = {
    "Name" = "${var.environment}-private"
  }

}