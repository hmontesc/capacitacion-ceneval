data "aws_vpc" "selected" {

  filter {
    name   = "tag:Project"
    values = ["terraform-learning"]
  }
}

data "aws_subnet" "private" {
  vpc_id = data.aws_vpc.selected.id

  filter {
    name   = "tag:Name"
    values = ["${var.environment}-private_1"]
  }
}

data "aws_subnet" "public" {
  vpc_id = data.aws_vpc.selected.id

  filter {
    name   = "tag:Name"
    values = ["${var.environment}-public_1"]
  }
}

data "aws_security_groups" "vpc" {

  filter {
    name   = "tag:Name"
    values = ["ssh-${var.environment}"]
  }
}